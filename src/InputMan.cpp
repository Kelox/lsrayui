#include <LsRayUI/InputMan.hpp>
#include <LsRayUI/LsRayUI.hpp>
#include <LsRayUI/Widgets/Widget.hpp>
#include <list>


// MACROS //
#define MOUSE_CLICK_THRESHOLD 16.0f
#define KEY_REPEAT_TIME 0.05f


namespace LsRayUI::InputMan
{
    // STRUCTS //
    typedef struct
    {
        int keycode;
        char keychar;
        double repeatTimestamp; // in s
    } pressedkey_t;
    
    
    // VARIABLES //
    static Widget* capturedWidget = nullptr;
    static Widget* hoverWidget = nullptr;
    
    static Vector2 mouseDownPos[3];
    static unsigned char mouseDownMask = 0;
    static unsigned char mouseClickMask = 0;
    
    static std::list<pressedkey_t> pressedKeyList;
    
    
    // PROTOTYPES //
    static void SetHoveringWidget(Widget* widget);
    
    static void OnMouseMove(float mx, float my, float dx, float dy);
    static void OnMouseDown(float mx, float my, enum MouseButton btn);
    static void OnMouseUp(float mx, float my, enum MouseButton btn);
    static void OnMouseClick(float mx, float my, enum MouseButton btn);
    static void OnMouseScroll(float mx, float my, float dx, float dy);
    
    static void OnKeyDown(int keycode, char keychar);
    static void OnKeyUp(int keycode, char keychar);
    static void OnKeyRepeat(int keycode, char keychar);
    
    static int GetMouseDownIndex(enum MouseButton btn);
    
    static void EmitTreeSignal(Widget* source, const char* name, ...);
    
    
    // PUBLIC FUNCTIONS //
    void Init()
    {
        for (int i = 0; i < 3; i++)
            mouseDownPos[i] = (Vector2){ 0, 0 };
    }
    
    void Free()
    {
    
    }
    
    void Update(float delta)
    {
        // Mouse move
        Vector2 mouseDelta = GetMousePosition();
        Vector2 mousePos = GetMousePosition();
        if (mouseDelta.x != 0 || mouseDelta.y != 0)
            OnMouseMove(mousePos.x, mousePos.y, mouseDelta.x, mouseDelta.y);
        
        // Mouse up/down
        if (IsMouseButtonPressed(MOUSE_BUTTON_LEFT))
            OnMouseDown(mousePos.x, mousePos.y, MouseButton::LS_MOUSE_BUTTON_LEFT);
        if (IsMouseButtonReleased(MOUSE_BUTTON_LEFT))
            OnMouseUp(mousePos.x, mousePos.y, MouseButton::LS_MOUSE_BUTTON_LEFT);
        if (IsMouseButtonPressed(MOUSE_BUTTON_MIDDLE))
            OnMouseDown(mousePos.x, mousePos.y, MouseButton::LS_MOUSE_BUTTON_MIDDLE);
        if (IsMouseButtonReleased(MOUSE_BUTTON_MIDDLE))
            OnMouseUp(mousePos.x, mousePos.y, MouseButton::LS_MOUSE_BUTTON_MIDDLE);
        if (IsMouseButtonPressed(MOUSE_BUTTON_RIGHT))
            OnMouseDown(mousePos.x, mousePos.y, MouseButton::LS_MOUSE_BUTTON_RIGHT);
        if (IsMouseButtonReleased(MOUSE_BUTTON_RIGHT))
            OnMouseUp(mousePos.x, mousePos.y, MouseButton::LS_MOUSE_BUTTON_RIGHT);
        
        // Mouse scroll
        Vector2 scrl = GetMouseWheelMoveV();
        if (scrl.x != 0 || scrl.y != 0)
            OnMouseScroll(mousePos.x, mousePos.y, scrl.x, scrl.y);
        
        // Detect new key presses
        int keycode = 0;
        while ((keycode = GetKeyPressed()) != 0)
        {
            int keychar = GetCharPressed();
            if (keychar > 0b01111111)
                continue;
            
            // Contained already?
            bool found = false;
            for (pressedkey_t& listKey : pressedKeyList)
            {
                if (listKey.keycode == keycode)
                {
                    found = true;
                    break;
                }
            }
            if (found)
                continue;
            
            // Press key
            pressedKeyList.push_back(
                (pressedkey_t){ keycode, (char)keychar, GetTime() + KEY_REPEAT_TIME * 3.0f }
            );
            OnKeyDown(keycode, (char)keychar);
            OnKeyRepeat(keycode, (char)keychar);
        }
        
        // Detect key releases
        for (int i = (int)pressedKeyList.size() - 1; i >= 0; i--)
        {
            // Get key
            auto iter = pressedKeyList.begin();
            std::advance(iter, i);
            pressedkey_t& listKey = *iter;
            
            // Check if still pressed
            if (IsKeyDown(listKey.keycode))
                continue;
            
            // Release key
            pressedKeyList.erase(iter);
            OnKeyUp(listKey.keycode, listKey.keychar);
        }
        
        // Detect key repeats
        for (pressedkey_t& listKey : pressedKeyList)
        {
            if ((GetTime() - listKey.repeatTimestamp) >= KEY_REPEAT_TIME)
            {
                listKey.repeatTimestamp = GetTime();
                OnKeyRepeat(listKey.keycode, listKey.keychar);
            }
        }
    }
    
    void SetCapturedWidget(Widget* widget)
    {
        if (widget == capturedWidget)
            return;
        capturedWidget = widget;
    }
    Widget* GetCapturedWidget()
    {
        return capturedWidget;
    }
    
    static void SetHoveringWidget(Widget* widget)
    {
        if (widget == hoverWidget)
            return;
        hoverWidget = widget;
        
        SetMouseCursor(hoverWidget ? hoverWidget->GetHoverCursor() : MOUSE_CURSOR_DEFAULT);
    }
    Widget* GetHoveringWidget()
    {
        return hoverWidget;
    }
    
    
    // PRIVATE FUNCTIONS //
    static void OnMouseMove(float mx, float my, float dx, float dy)
    {
        // Get affected widget
        Widget* widget = nullptr;
        if (capturedWidget)
        {
            widget = capturedWidget;
        }
        else if (LsRayUI::HasRootWidget() && LsRayUI::GetRootWidget()->GetEnabled())
        {
            widget = LsRayUI::GetRootWidget()->GetWidgetAt(mx, my, true);
        }
        
        // Unhover widget signal
        if (GetHoveringWidget() && widget != GetHoveringWidget())
        {
            Rectangle rect = GetHoveringWidget()->Measure();
            EmitTreeSignal(
                GetHoveringWidget(),
                "mouse-unhover",
                "x", Signal::AsParam(mx - rect.x),
                "y", Signal::AsParam(my - rect.y),
                nullptr
            );
            SetHoveringWidget(nullptr);
        }
        
        // Skip if no widget
        if (!widget)
            return;
        
        // Hover widget signal
        if (widget != GetHoveringWidget())
        {
            SetHoveringWidget(widget);
            Rectangle rect = GetHoveringWidget()->Measure();
            EmitTreeSignal(
                GetHoveringWidget(),
                "mouse-hover",
                "x", Signal::AsParam(mx - rect.x),
                "y", Signal::AsParam(my - rect.y),
                nullptr
            );
        }
        
        // Move widget signal
        Rectangle rect = widget->Measure();
        EmitTreeSignal(
            widget,
            "mouse-move",
            "x", Signal::AsParam(mx - rect.x),
            "y", Signal::AsParam(my - rect.y),
            "dx", Signal::AsParam(dx),
            "dy", Signal::AsParam(dy),
            nullptr
        );
        
        // Click?
        for (unsigned char btn : {
            MouseButton::LS_MOUSE_BUTTON_LEFT,
            MouseButton::LS_MOUSE_BUTTON_MIDDLE,
            MouseButton::LS_MOUSE_BUTTON_RIGHT })
        {
            if (!(mouseDownMask & btn) || !(mouseClickMask & btn))
                continue;
            
            // Get down pos
            Vector2 downPos = mouseDownPos[GetMouseDownIndex((enum MouseButton)btn)];
            
            // Check if we passed the click threshold
            if (Common::Dist2d(downPos.x, downPos.y, mx, my) > MOUSE_CLICK_THRESHOLD)
            {
                mouseClickMask ^= btn;
            }
        }
    }
    
    static void OnMouseDown(float mx, float my, enum MouseButton btn)
    {
        // Get affected widget
        Widget* widget = nullptr;
        if (capturedWidget)
        {
            widget = capturedWidget;
        }
        else if (LsRayUI::HasRootWidget() && LsRayUI::GetRootWidget()->GetEnabled())
        {
            widget = LsRayUI::GetRootWidget()->GetWidgetAt(mx, my, true);
        }
        if (!widget)
            return;
        
        // Emit signal
        Rectangle rect = widget->Measure();
        EmitTreeSignal(
            widget,
            "mouse-down",
            "x", Signal::AsParam(mx - rect.x),
            "y", Signal::AsParam(my - rect.y),
            "button", Signal::AsParam(btn),
            nullptr
        );
        
        // Etc
        mouseDownPos[GetMouseDownIndex(btn)].x = mx;
        mouseDownPos[GetMouseDownIndex(btn)].y = my;
        mouseDownMask |= (unsigned char)btn;
        mouseClickMask |= (unsigned char)btn;
    }
    
    static void OnMouseUp(float mx, float my, enum MouseButton btn)
    {
        // Get affected widget
        Widget* widget = nullptr;
        if (capturedWidget)
        {
            widget = capturedWidget;
        }
        else if (LsRayUI::HasRootWidget() && LsRayUI::GetRootWidget()->GetEnabled())
        {
            widget = LsRayUI::GetRootWidget()->GetWidgetAt(mx, my, true);
        }
        if (!widget)
            return;
        
        // Emit signal
        Rectangle rect = widget->Measure();
        EmitTreeSignal(
            widget,
            "mouse-up",
            "x", Signal::AsParam(mx - rect.x),
            "y", Signal::AsParam(my - rect.y),
            "button", Signal::AsParam(btn),
            nullptr
        );
        
        // Click
        if (mouseClickMask & btn)
            OnMouseClick(mx, my, btn);
        
        // Etc
        mouseDownMask &= ~btn;
    }
    
    static void OnMouseClick(float mx, float my, enum MouseButton btn)
    {
        // Get affected widget
        Widget* widget = nullptr;
        if (capturedWidget)
        {
            widget = capturedWidget;
        }
        else if (LsRayUI::HasRootWidget() && LsRayUI::GetRootWidget()->GetEnabled())
        {
            widget = LsRayUI::GetRootWidget()->GetWidgetAt(mx, my, true);
        }
        if (!widget)
            return;
        
        // Emit signal
        Rectangle rect = widget->Measure();
        EmitTreeSignal(
            widget,
            "mouse-click",
            "x", Signal::AsParam(mx - rect.x),
            "y", Signal::AsParam(my - rect.y),
            "button", Signal::AsParam(btn),
            nullptr
        );
    }
    
    static void OnMouseScroll(float mx, float my, float dx, float dy)
    {
        // Get affected widget
        Widget* widget = nullptr;
        if (capturedWidget)
        {
            widget = capturedWidget;
        }
        else if (LsRayUI::HasRootWidget() && LsRayUI::GetRootWidget()->GetEnabled())
        {
            widget = LsRayUI::GetRootWidget()->GetWidgetAt(mx, my, true);
        }
        if (!widget)
            return;
        
        // Emit signal
        Rectangle rect = widget->Measure();
        EmitTreeSignal(
            widget,
            "mouse-scroll",
            "x", Signal::AsParam(mx - rect.x),
            "y", Signal::AsParam(my - rect.y),
            "dx", Signal::AsParam(dx),
            "dy", Signal::AsParam(dy),
            nullptr
        );
    }
    
    static void OnKeyDown(int keycode, char keychar)
    {
        // Get affected widget
        Widget* widget = nullptr;
        if (GetCapturedWidget())
            widget = GetCapturedWidget();
        else
            widget = GetHoveringWidget();
        if (!widget)
            return;
        
        // Emit signal
        EmitTreeSignal(
            widget,
            "key-down",
            "keycode", Signal::AsParam(keycode),
            "keychar", Signal::AsParam(keychar),
            nullptr
        );
    }
    
    static void OnKeyUp(int keycode, char keychar)
    {
        // Get affected widget
        Widget* widget = nullptr;
        if (GetCapturedWidget())
            widget = GetCapturedWidget();
        else
            widget = GetHoveringWidget();
        if (!widget)
            return;
        
        // Emit signal
        EmitTreeSignal(
            widget,
            "key-up",
            "keycode", Signal::AsParam(keycode),
            "keychar", Signal::AsParam(keychar),
            nullptr
        );
    }
    
    static void OnKeyRepeat(int keycode, char keychar)
    {
        // Get affected widget
        Widget* widget = nullptr;
        if (GetCapturedWidget())
            widget = GetCapturedWidget();
        else
            widget = GetHoveringWidget();
        if (!widget)
            return;
        
        // Emit signal
        EmitTreeSignal(
            widget,
            "key-input",
            "keycode", Signal::AsParam(keycode),
            "keychar", Signal::AsParam(keychar),
            nullptr
        );
    }
    
    static int GetMouseDownIndex(enum MouseButton btn)
    {
        switch (btn)
        {
            case MouseButton::LS_MOUSE_BUTTON_LEFT:
                return 0;
            case MouseButton::LS_MOUSE_BUTTON_MIDDLE:
                return 1;
            case MouseButton::LS_MOUSE_BUTTON_RIGHT:
                return 2;
        }
        return 0;
    }
    
    static void EmitTreeSignal(Widget* source, const char* name, ...)
    {
        // TODO: if captured, maybe only send to the captured widget
        
        if (!source)
            return;
        
        // Get all affected widgets
        std::list<Widget*> affList;
        do
        {
            affList.push_front(source);
            source = source->GetParent();
        } while (source);
        
        // Emit signal multiple times
        for (auto iter = affList.begin(); iter != affList.end(); iter++)
        {
            Widget* widget = *iter;
            va_list list;
            va_start(list, name);
            Signal::Emit(widget, name, list);
            va_end(list);
        }
    }
}