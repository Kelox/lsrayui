#include <LsRayUI/Common.hpp>
#include <list>
#include <cstdlib>
#include <stdexcept>
#include <raylib.h>
#include <cmath>
#include <algorithm>


namespace LsRayUI::Common
{
    // VARIABLES //
    std::list<Rectangle*> scissorStack;
    
    
    // PROTOTYPES //
    static void ApplyScissor();
    
    
    // PUBLIC FUNCTIONS //
    void PushScissor(float x, float y, float w, float h)
    {
        // Allocate rectangle
        Rectangle* rect = (Rectangle*)malloc(sizeof(Rectangle));
        if (!rect)
        {
            TraceLog(LOG_ERROR, "Malloc returned null");
            abort();
        }
        
        rect->x = x;
        rect->y = y;
        rect->width = w;
        rect->height = h;
        
        // End scissor if it was active already
        if (!scissorStack.empty())
            EndScissorMode();
        
        // Add rect to stack
        scissorStack.push_back(rect);
        
        ApplyScissor();
    }
    
    void PopScissor()
    {
        if (scissorStack.empty())
        {
            throw std::runtime_error(
                "Tried to pop scissor while there was none"
            );
        }
        
        Rectangle* rect = scissorStack.back();
        scissorStack.remove(rect);
        free(rect);
        
        ApplyScissor();
    }
    
    bool HasScissor()
    {
        return !scissorStack.empty();
    }
    
    float Dist2d(float x1, float y1, float x2, float y2)
    {
        return sqrtf((x2 - x1) * (x2 - x1) + (y2 - y1) * (y2 - y1));
    }
    
    void DrawShadowedText(Font font, const char* text, float fontSize,
                          float posX, float posY, float spacing,
                          float localPosX, float localPosY,
                          float shadowOffset, Color tint)
    {
        Vector2 txtSize = MeasureTextEx(font, text, fontSize, spacing);
        posX -= txtSize.x * localPosX;
        posY -= txtSize.y * localPosY;
        
        float shadowAlpha = ((float)tint.a / 255.0f) * 125.0f;
        DrawTextEx(
            font, text,
            (Vector2){ posX + shadowOffset, posY + shadowOffset },
            fontSize,
            spacing,
            (Color){ 0, 0, 0, (unsigned char)shadowAlpha }
        );
        
        DrawTextEx(
            font, text,
            (Vector2){ posX, posY },
            fontSize,
            spacing,
            tint
        );
    }
    
    void DrawMultilineText(Font font, float fontSize, float fontHeight, float spacing,
                           const char* text, float posX, float posY, Color tint)
    {
        std::string str(text);
        int lines = GetStringLineCount(str);
        
        for (int i = 0; i < lines; i++)
        {
            std::string line = GetStringLine(str, i);
            DrawTextEx(
                font, line.c_str(),
                (Vector2){ posX, posY + (float)i * fontHeight },
                fontSize, spacing, tint
            );
        }
    }
    
    Vector2 MeasureMultilineText(Font font, float fontSize, float fontHeight, float spacing,
                                 const char* text)
    {
        Vector2 size = (Vector2){ 0, 0 };
        
        std::string str(text);
        int lines = GetStringLineCount(str);
        size.y = (float)lines * fontHeight;
        
        for (int i = 0; i < lines; i++)
        {
            std::string line = GetStringLine(str, i);
            size.x = std::max(size.x, MeasureTextEx(font, line.c_str(), fontSize, spacing).x);
        }
        
        return size;
    }
    
    std::string GetStringLine(std::string& str, int line,
                              int* outStart, int* outEnd)
    {
        if (outStart)
            *outStart = 0;
        if (outEnd)
            *outEnd = 0;
        
        if (line < 0)
            return "";
        
        // Find line start
        int ptr = 0;
        while (line > 0)
        {
            if (str[ptr] == '\0')
                return "";
            else if (str[ptr] == '\n')
                line--;
            ptr++;
        }
        
        // Find line end
        int end;
        for (end = ptr; end <= str.length(); end++)
        {
            char idk = str[end];
            if (str[end] == '\0' || str[end] == '\n')
                break;
        }
        
        // Return substring
        std::string res = str.substr(ptr, end - ptr + 1);
        res[end - ptr] = '\0';
        
        if (outStart)
            *outStart = ptr;
        if (outEnd)
            *outEnd = end;
        return res;
    }
    
    int GetStringLineCount(std::string& str)
    {
        return (int)std::count(str.begin(), str.end(), '\n') + 1;
    }
    
    int GetStringLineAtIndex(std::string& str, int idx)
    {
        int lines = GetStringLineCount(str);
        if (idx <= 0)
            return 0;
        
        for (int i = 0; i < lines; i++)
        {
            int start, end;
            (void)GetStringLine(str, i, &start, &end);
            if (idx >= start && idx <= end)
                return i;
        }
        
        return lines - 1;
    }
    
    
    // PRIVATE FUNCTIONS //
    static void ApplyScissor()
    {
        // Determine new scissor area
        float scissorLeft = -999999;
        float scissorRight = 999999;
        float scissorTop = -999999;
        float scissorBottom = 999999;
        for (Rectangle* scissorRect : scissorStack)
        {
            scissorLeft = std::max(scissorLeft, scissorRect->x);
            scissorRight = std::min(scissorRight, scissorRect->x + scissorRect->width);
            scissorTop = std::max(scissorTop, scissorRect->y);
            scissorBottom = std::min(scissorBottom, scissorRect->y + scissorRect->height);
        }
        
        // Begin scissor mode
        BeginScissorMode(
            (int)scissorLeft, (int)scissorTop,
            (int)(scissorRight - scissorLeft),
            (int)(scissorBottom - scissorTop)
        );
    }
}