#include <LsRayUI/Signal.hpp>
#include <list>
#include <cstdlib>
#include <cstdio>


namespace LsRayUI::Signal
{
    // STRUCTS //
    typedef struct
    {
        void* source;
        char name[SIG_NAME_LEN];
        callbackFunc targetFn;
        void* target;
    } sigconnection_t;
    
    
    // VARIABLES //
    static std::list<sigconnection_t*> conList;
    
    
    // PUBLIC FUNCTIONS //
    void Connect(void* source, const char* name, callbackFunc targetFn, void* target)
    {
        sigconnection_t* con = (sigconnection_t*)malloc(sizeof(sigconnection_t));
        if (!con)
        {
            printf("Malloc failed");
            abort();
        }
        con->source = source;
        strncpy(con->name, name, SIG_NAME_LEN);
        con->targetFn = targetFn;
        con->target = target;
        
        conList.push_back(con);
        
        printf("Connected signal '%s'\n", name);
    }
    
    void Disconnect(void* source, const char* name, void* target)
    {
        for (int i = conList.size() - 1; i >= 0; i--)
        {
            // Get connection
            auto iter = conList.begin();
            std::advance(iter, i);
            sigconnection_t* con = *iter;
            
            // Skip if not the right one
            if (con->source != source || con->target != target ||
                strncmp(con->name, name, SIG_NAME_LEN) != 0)
            {
                continue;
            }
            
            // Delete entry
            conList.erase(iter);
            free(con);
        }
        
        printf("Disconnected signal '%s'\n", name);
    }
    
    void Emit(void* source, const char* name, ...)
    {
        va_list list;
        va_start(list, name);
        Emit(source, name, list);
        va_end(list);
    }
    
    void Emit(void* source, const char* name, va_list list)
    {
        // Prepare params list
        sigparams_t* params = nullptr;
        while (true)
        {
            // Retrieve name & value
            const char* paramName = va_arg(list, const char*);
            if (!paramName)
                break;
            uintptr_t paramValue = va_arg(list, uintptr_t);
            
            // Create new param node
            sigparams_t* ph = (sigparams_t*)malloc(sizeof(sigparams_t));
            if (!ph)
            {
                printf("Malloc failed");
                abort();
            }
            strncpy(ph->name, paramName, SIG_PARAM_NAME_LEN);
            ph->value = paramValue;
            ph->next = nullptr;
            
            // Add to list
            if (params)
            {
                sigparams_t* last = params;
                while (last->next)
                    last = last->next;
                last->next = ph;
            }
            else
            {
                params = ph;
            }
        }
        
        // Execute all handlers
        for (auto iter = conList.begin(); iter != conList.end(); iter++)
        {
            sigconnection_t* con = *iter;
            if (con->source != source || strncmp(con->name, name, SIG_NAME_LEN) != 0)
                continue;
            con->targetFn(con->target, params);
        }
        
        // Free params
        while (params)
        {
            sigparams_t* next = params->next;
            free(params);
            params = next;
        }
    }
    
    bool HasParam(sigparams_t* params, const char* name)
    {
        while (params != nullptr && strncmp(params->name, name, SIG_PARAM_NAME_LEN) != 0)
            params = params->next;
        return params != nullptr;
    }
    
    void Finalize()
    {
        while (!conList.empty())
        {
            sigconnection_t* con = conList.front();
            char conName[SIG_NAME_LEN];
            strncpy(conName, con->name, SIG_NAME_LEN);
            Disconnect(con->source, conName, con->target);
        }
    }
}