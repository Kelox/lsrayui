#include <LsRayUI/LsRayUI.hpp>
#include <LsRayUI/InputMan.hpp>


namespace LsRayUI
{
    // VARIABLES //
    static int screenWidth = 800;
    static int screenHeight = 600;
    
    static Widget* rootWidget = nullptr;
    
    
    // PUBLIC FUNCTIONS //
    void Init()
    {
        InputMan::Init();
    }
    
    void Free()
    {
        if (HasRootWidget())
        {
            throw std::runtime_error(
                "Called free before removing the root widget"
            );
        }
        
        Signal::Finalize();
        InputMan::Free();
    }
    
    void Update(float delta)
    {
        // Update widget
        if (HasRootWidget())
            GetRootWidget()->Update(delta);
        
        // Update input
        InputMan::Update(delta);
    }
    
    void Draw()
    {
        // Draw widget
        if (HasRootWidget())
            GetRootWidget()->Draw();
        
        // Check if scissor was not cleared properly
        if (Common::HasScissor())
        {
            throw std::runtime_error(
                "Scissor was pushed more often than popped"
            );
        }
    }
    
    
    void SetScreenSize(int width, int height)
    {
        screenWidth = width;
        screenHeight = height;
    }
    
    int GetScreenWidth()
    {
        return screenWidth;
    }
    
    int GetScreenHeight()
    {
        return screenHeight;
    }
    
    
    bool HasRootWidget()
    {
        return GetRootWidget() != nullptr;
    }
    
    Widget* GetRootWidget()
    {
        return rootWidget;
    }
    
    void SetRootWidget(Widget* widget)
    {
        if (HasRootWidget())
        {
            throw std::runtime_error(
                "Tried to set a new root widget while another was active"
            );
        }
        else if (widget == nullptr)
        {
            throw std::runtime_error(
                "Tried to set the root widget to null"
            );
        }
        
        rootWidget = widget;
    }
    
    Widget* RemoveRootWidget()
    {
        if (!HasRootWidget())
        {
            throw std::runtime_error(
                "There is no root widget to remove"
            );
        }
        
        Widget* ph = rootWidget;
        rootWidget = nullptr;
        return ph;
    }
}