#include <LsRayUI/Widgets/Window.hpp>
#include <LsRayUI/Common.hpp>


// MACROS //
#define WINDOW_ROUNDNESS 8.0f
#define WINDOW_BORDER_THICKNESS 5.0f


namespace LsRayUI
{
    // CONSTRUCTOR / DECONSTRUCTOR //
    Window::Window() : Window(0, 0, 0, 0) { }
    
    Window::Window(float px, float py, float pWidth, float pHeight) :
        Widget(px, py, pWidth, pHeight)
    {
        padding = 0;
    }
    
    Window::~Window() = default;
    
    
    /// PUBLIC METHODS //
    void Window::Update(float delta)
    {
        // Chain update loop
        for (int i = 0; i < GetChildCount(); i++)
            Widget::GetChild(i)->Update(delta);
    }
    
    void Window::Draw()
    {
        // Get rectangle
        Rectangle rect = Measure();
        if (!IsVisible(rect))
            return;
        
        // Determine roundness
        float roundness = rect.width < rect.height ? rect.width : rect.height;
        roundness = Common::Clamp<float>(
            (1.0f / roundness) * WINDOW_ROUNDNESS,
            0, 1
        );
        
        // Draw
        DrawRectangleRounded(
            rect,
            roundness,
            16,
            (Color){ 35, 130, 155, 40 }
        );
        
        DrawRectangleRoundedLines(
            (Rectangle){
                rect.x + WINDOW_BORDER_THICKNESS / 2.0f,
                rect.y + WINDOW_BORDER_THICKNESS / 2.0f,
                rect.width, rect.height
            },
            roundness,
            16,
            WINDOW_BORDER_THICKNESS,
            (Color){ 0, 0, 0, 255 }
        );
        DrawRectangleRoundedLines(
            rect,
            roundness,
            16,
            WINDOW_BORDER_THICKNESS,
            (Color){ 130, 180, 200, 255 }
        );
        
        if (GetChildCount() > 0)
        {
            // Push content clip
            Common::PushScissor(rect.x, rect.y, rect.width, rect.height);
            
            // Draw child
            GetChild()->Draw();
            
            // Pop clip
            Common::PopScissor();
        }
    }
    
    void Window::SetChild(Widget* child)
    {
        if (!childList.empty())
        {
            throw std::runtime_error(
                "A Window may only have one child at a time"
            );
        }
        
        if (child)
            AddChild(child);
        else
            RemoveChild(child);
    }
    
    Widget* Window::GetChild()
    {
        return Widget::GetChild(0);
    }
    
    void Window::SetPadding(float pPadding)
    {
        padding = pPadding;
    }
    
    
    // PROTECTED METHODS //
    void Window::MeasureChild(Widget* child)
    {
        child->x = padding;
        child->y = padding;
        child->width = Common::Clamp<float>(
            width - padding * 2.0f, 0, width
        );
        child->height = Common::Clamp<float>(
            height - padding * 2.0f, 0, height
        );
    }
}