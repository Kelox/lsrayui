#include <LsRayUI/Widgets/Entry.hpp>
#include <LsRayUI/Common.hpp>
#include <LsRayUI/InputMan.hpp>
#include <algorithm>
#include <cmath>


// MACROS //
#define ENTRY_ROUNDNESS 8.0f
#define ENTRY_SHADOW_OFFSET 4.0f
#define ENTRY_FONT_SIZE 16.0f
#define ENTRY_FONT_HEIGHT (ENTRY_FONT_SIZE * 1.5f)
#define ENTRY_SELECTION_COLOR (Color){ 100, 255, 100, 100 }
#define ENTRY_SELECTION_BLINK_TIME 0.25f

#define INIT_DRAW_TEXT std::string* drawText = &text; \
    std::string phHiddenDrawText; \
    if (text.empty()) { \
        drawText = &hint; \
    } else if (hidden) { \
        phHiddenDrawText = std::string(text.length(), '*'); \
        drawText = &phHiddenDrawText; \
        size_t idx = -1; \
        while ((idx = text.find('\n', idx + 1)) != std::string::npos) { \
            phHiddenDrawText[idx] = '\n'; \
        } \
    }


namespace LsRayUI
{
    // CONSTRUCTOR / DECONSTRUCTOR //
    Entry::Entry(const char* pText, const char* pHint, bool pSingleLine)
    {
        text = std::string(pText);
        hint = std::string(pHint);
        singleLine = pSingleLine;
        hidden = false;
        
        padding.left = 8;
        padding.right = 8;
        padding.top = 8;
        padding.bottom = 8;
        
        selection[0] = text.length();
        selection[1] = selection[0];
        
        selectionBlink = false;
        selectionBlinkTimer = 0;
        SetHoverCursor(MOUSE_CURSOR_IBEAM);
        
        prevTextLength = -1;
        
        txtWidth = 0;
        txtHeight = 0;
        
        Signal::Connect(this, "mouse-move", &Entry::SigMouseMoveCb, this);
        Signal::Connect(this, "mouse-down", &Entry::SigMouseDownCb, this);
        Signal::Connect(this, "mouse-up", &Entry::SigMouseUpCb, this);
        Signal::Connect(this, "key-input", &Entry::SigKeyInputCb, this);
    }
    
    Entry::~Entry()
    {
    
    }
    
    
    // PUBLIC METHODS //
    void Entry::Update(float delta)
    {
        if (InputMan::GetCapturedWidget() == this || InputMan::GetHoveringWidget() == this)
        {
            selectionBlinkTimer += delta;
            if (selectionBlinkTimer >= ENTRY_SELECTION_BLINK_TIME)
            {
                selectionBlinkTimer = 0;
                selectionBlink = !selectionBlink;
            }
        }
        
        // Update default size
        if (prevTextLength != text.length())
        {
            Vector2 size = Common::MeasureMultilineText(
                GetFontDefault(),
                ENTRY_FONT_SIZE,
                ENTRY_FONT_HEIGHT,
                1.0f,
                text.c_str()
            );
            
            prevTextLength = (int)text.length();
            txtWidth = size.x;
            txtHeight = size.y;
        }
        /*SetDefaultSize(
            std::max(txtWidth, ENTRY_FONT_HEIGHT), std::max(txtHeight, ENTRY_FONT_HEIGHT)
        );*/
        
        // Update single line padding
        if (singleLine)
        {
            Rectangle rect = Measure();
            if (IsVisible(rect))
            {
                padding.top = (rect.height - ENTRY_FONT_HEIGHT) / 2.0f;
                padding.bottom = (rect.height - ENTRY_FONT_HEIGHT) / 2.0f;
            }
        }
    }
    
    void Entry::Draw()
    {
        // Get rectangle
        Rectangle rect = Measure();
        if (!IsVisible(rect))
            return;
        
        // Determine roundness
        float roundness = rect.width < rect.height ? rect.width : rect.height;
        roundness = Common::Clamp<float>(
            (1.0f / roundness) * ENTRY_ROUNDNESS,
            0, 1
        );
        
        // Draw background
        DrawRectangleRounded(
            (Rectangle){
                rect.x + ENTRY_SHADOW_OFFSET,
                rect.y + ENTRY_SHADOW_OFFSET,
                rect.width, rect.height
            },
            roundness,
            16,
            (Color){ 0, 0, 0, 155 }
        );
        DrawRectangleRounded(
            (Rectangle){
                rect.x, rect.y,
                rect.width, rect.height
            },
            roundness,
            16,
            (Color){ 255, 255, 255, 255 }
        );
        
        Common::PushScissor(
            rect.x + padding.left,
            rect.y + padding.top,
            rect.width - padding.left - padding.right,
            rect.height - padding.top - padding.bottom
        );
        
        // Get draw text
        INIT_DRAW_TEXT;
        Color drawTextColor = BLACK;
        if (text.empty())
            drawTextColor = GRAY;
        
        // Draw text
        Common::DrawMultilineText(
            GetFontDefault(), ENTRY_FONT_SIZE, ENTRY_FONT_HEIGHT, 1.0f,
            drawText->c_str(),
            rect.x + padding.left,
            rect.y + padding.top,
            drawTextColor
        );
        
        // Get selection marker positions
        float markerX[2], markerY[2];
        for (int i = 0; i < 2; i++)
            GetPixelPosition(selection[i], &markerX[i], &markerY[i]);
        
        // Draw selection areas
        if (selection[0] != selection[1])
        {
            if (GetLineOfIndex(selection[0]) != GetLineOfIndex(selection[1]))
            {
                // Get ordered selection
                int phSel[2];
                float phX[2], phY[2];
                int phIndex = selection[0] < selection[1] ? 0 : 1;
                phSel[0] = selection[phIndex];
                phX[0] = markerX[phIndex];
                phY[0] = markerY[phIndex];
                phSel[1] = selection[1 - phIndex];
                phX[1] = markerX[1 - phIndex];
                phY[1] = markerY[1 - phIndex];
                
                // Draw start rect
                DrawRectangle(
                    (int)(phX[0] + rect.x),
                    (int)(phY[0] + rect.y),
                    (int)std::abs(width - phX[0]),
                    (int)ENTRY_FONT_HEIGHT,
                    ENTRY_SELECTION_COLOR
                );
                
                // Draw end rect
                DrawRectangle(
                    (int)rect.x,
                    (int)(phY[1] + rect.y),
                    (int)phX[1],
                    (int)ENTRY_FONT_HEIGHT,
                    ENTRY_SELECTION_COLOR
                );
                
                // Draw fill rect
                DrawRectangle(
                    (int)rect.x,
                    (int)(rect.y + phY[0] + ENTRY_FONT_HEIGHT),
                    (int)width,
                    (int)(phY[1] - phY[0] - ENTRY_FONT_HEIGHT),
                    ENTRY_SELECTION_COLOR
                );
            }
            else
            {
                DrawRectangle(
                    (int)(std::min(markerX[0], markerX[1]) + rect.x),
                    (int)(markerY[0] + rect.y),
                    (int)std::abs(markerX[1] - markerX[0]),
                    (int)ENTRY_FONT_HEIGHT,
                    ENTRY_SELECTION_COLOR
                );
            }
        }
        else if (selectionBlink)
        {
            DrawRectangle(
                (int)(markerX[0] + rect.x), (int)(markerY[0] + rect.y),
                2, (int)ENTRY_FONT_HEIGHT, BLACK
            );
        }
        
        Common::PopScissor();
    }
    
    void Entry::SetHidden(bool pHidden)
    {
        hidden = pHidden;
    }
    
    bool Entry::GetHidden()
    {
        return hidden;
    }
    
    
    // PRIVATE METHODS //
    int Entry::GetCharPosition(float pixelX, float pixelY)
    {
        pixelX -= padding.left;
        pixelY -= padding.top;
        
        // Count lines
        int lines = (int)std::count(text.begin(), text.end(), '\n') + 1;
        
        // Get row pos
        int row = (int)floorf(pixelY / ENTRY_FONT_HEIGHT);
        if (row >= lines)
        {
            row = lines - 1;
            pixelX = 999999;
        }
        else if (row < 0)
        {
            row = 0;
            pixelX = 0;
        }
        
        // Get line string
        int lineStart, lineEnd;
        INIT_DRAW_TEXT;
        if (drawText == &hint)
            drawText = &text;
        std::string line = Common::GetStringLine(*drawText, row, &lineStart, &lineEnd);
        
        // Find selected index
        int left = 0;
        int right = (int)line.length();
        int mid = (right - left) / 2 + left;
        while ((right - left) > 1)
        {
            std::string subMid = line.substr(0, mid);
            
            float midWidth = MeasureTextEx(
                GetFontDefault(), subMid.c_str(), ENTRY_FONT_SIZE, 1.0f
            ).x;
            
            if (pixelX >= midWidth)
                left = mid;
            else
                right = mid;
            mid = (right - left) / 2 + left;
        }
        
        std::string subLeft = line.substr(0, left);
        std::string subRight = line.substr(0, right);
        float leftWidth = MeasureTextEx(
            GetFontDefault(), subLeft.c_str(), ENTRY_FONT_SIZE, 1.0f
        ).x;
        float rightWidth = MeasureTextEx(
            GetFontDefault(), subRight.c_str(), ENTRY_FONT_SIZE, 1.0f
        ).x;
        if (fabsf(pixelX - leftWidth) > fabsf(pixelX - rightWidth))
        {
            left = right;
            leftWidth = rightWidth;
        }
        
        return lineStart + left;
    }
    
    void Entry::GetPixelPosition(int charPos, float* outPixelX, float* outPixelY)
    {
        *outPixelX = 0;
        *outPixelY = 0;
        
        INIT_DRAW_TEXT;
        if (drawText == &hint)
            drawText = &text;
        int lines = Common::GetStringLineCount(*drawText);
        for (int i = 0; i < lines; i++)
        {
            int lineStart, lineEnd;
            std::string line = Common::GetStringLine(*drawText, i, &lineStart, &lineEnd);
            
            if (charPos < lineStart || charPos > lineEnd)
                continue;
            
            *outPixelY = (float)i * ENTRY_FONT_HEIGHT;
            
            std::string sub = line.substr(0, charPos - lineStart);
            *outPixelX = Common::MeasureMultilineText(
                GetFontDefault(), ENTRY_FONT_SIZE, ENTRY_FONT_HEIGHT, 1.0f, sub.c_str()
            ).x;
        }
        
        *outPixelX += padding.left;
        *outPixelY += padding.top;
    }
    
    void Entry::SigMouseMoveCb(void* self, Signal::sigparams_t* params)
    {
        reinterpret_cast<Entry*>(self)->SigMouseMove(params);
    }
    
    void Entry::SigMouseMove(Signal::sigparams_t* params)
    {
        if (!IsCapturingInput())
            return;
        
        selection[1] = GetCharPosition(
            Signal::GetParam<float>(params, "x"), Signal::GetParam<float>(params, "y")
        );
    }
    
    void Entry::SigMouseDownCb(void* self, Signal::sigparams_t* params)
    {
        reinterpret_cast<Entry*>(self)->SigMouseDown(params);
    }
    void Entry::SigMouseDown(Signal::sigparams_t* params)
    {
        if (!CanCaptureInput())
            return;
        CaptureInput();
        
        selection[0] = GetCharPosition(
            Signal::GetParam<float>(params, "x"), Signal::GetParam<float>(params, "y")
        );
        selection[1] = selection[0];
        selectionBlink = true;
        selectionBlinkTimer = 0;
    }
    
    void Entry::SigMouseUpCb(void* self, Signal::sigparams_t* params)
    {
        reinterpret_cast<Entry*>(self)->SigMouseUp(params);
    }
    void Entry::SigMouseUp(Signal::sigparams_t* params)
    {
        if (!IsCapturingInput())
            return;
        ReleaseCaptureInput();
    }
    
    void Entry::SigKeyInputCb(void* self, Signal::sigparams_t* params)
    {
        reinterpret_cast<Entry*>(self)->SigKeyInput(params);
    }
    void Entry::SigKeyInput(Signal::sigparams_t* params)
    {
        static char insertTxt[2];
        insertTxt[1] = '\0';
        
        int keycode = Signal::GetParam<int>(params, "keycode");
        char keychar = Signal::GetParam<char>(params, "keychar");
        
        // Erase selected text
        if (selection[0] != selection[1])
        {
            if (selection[1] < selection[0])
            {
                int ph = selection[0];
                selection[0] = selection[1];
                selection[1] = ph;
            }
            
            if (keychar != '\0' || keycode == KEY_BACKSPACE ||
                keycode == KEY_ENTER || keycode == KEY_KP_ENTER)
            {
                int len = std::abs(selection[1] - selection[0]);
                text.erase(selection[0], len);
                selection[1] = selection[0];
                
                if (keycode == KEY_BACKSPACE)
                    return;
            }
            else
            {
                if (keycode == KEY_RIGHT || keycode == KEY_DOWN)
                    selection[0] = selection[1];
                else
                    selection[1] = selection[0];
                return;
            }
        }
        
        
        switch (keycode)
        {
            case KEY_ENTER:
            case KEY_KP_ENTER:
                if (singleLine)
                    return;
                insertTxt[0] = '\n';
                break;
            case KEY_LEFT:
                selection[0] = Common::Clamp(selection[0] - 1, 0, (int)text.length());
                selection[1] = selection[0];
                selectionBlinkTimer = 0;
                selectionBlink = true;
                return;
            case KEY_RIGHT:
                selection[0] = Common::Clamp(selection[0] + 1, 0, (int)text.length());
                selection[1] = selection[0];
                selectionBlinkTimer = 0;
                selectionBlink = true;
                return;
            case KEY_UP:
            case KEY_DOWN:
            {
                // Get numbers
                int lineCount = Common::GetStringLineCount(text);
                int curlineNumber = Common::GetStringLineAtIndex(text, selection[0]);
                
                // Skip if out of bounds
                if (curlineNumber <= 0 && keycode == KEY_UP ||
                    curlineNumber >= (lineCount - 1) && keycode == KEY_DOWN)
                {
                    return;
                }
                
                // Get current line
                int curlineStart, curlineEnd;
                std::string curline = Common::GetStringLine(
                    text, curlineNumber, &curlineStart, &curlineEnd
                );
                
                // Get new line
                int newlineStart, newlineEnd;
                std::string topline = Common::GetStringLine(
                    text, curlineNumber + (keycode == KEY_UP ? -1 : 1), &newlineStart, &newlineEnd
                );
                
                int relSelection = selection[0] - curlineStart;
                selection[0] = Common::Clamp(
                    newlineStart + relSelection, newlineStart, newlineEnd
                );
                selection[1] = selection[0];
                selectionBlinkTimer = 0;
                selectionBlink = true;
            }
                return;
            case KEY_BACKSPACE:
                if (selection[0] > 0)
                {
                    selection[0]--;
                    SigKeyInput(params);
                }
                return;
            default:
                insertTxt[0] = keychar;
                if (insertTxt[0] == '\0')
                    return;
                break;
        }
        
        // Add character
        text.insert(selection[0], insertTxt);
        selection[0]++;
        
        // Add indent
        if (insertTxt[0] == '\n')
        {
            int lineNo = GetLineOfIndex(selection[0]);
            if (lineNo > 0)
            {
                std::string line = Common::GetStringLine(text, lineNo - 1);
                int spaces;
                for (spaces = 0; spaces < line.length(); spaces++)
                {
                    if (line[spaces] != ' ')
                        break;
                }
                
                
                if (spaces > 0)
                {
                    text.insert(selection[0], std::string(spaces, ' '));
                    selection[0] += spaces;
                }
            }
        }
        
        // Set selection
        selection[1] = selection[0];
    }
    
    int Entry::GetLineOfIndex(int idx)
    {
        int lines = Common::GetStringLineCount(text);
        for (int i = 0; i < lines; i++)
        {
            int lineStart, lineEnd;
            (void)Common::GetStringLine(text, i, &lineStart, &lineEnd);
            if (idx >= lineStart && idx <= lineEnd)
                return i;
        }
        return 0;
    }
}