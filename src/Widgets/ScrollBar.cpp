#include <LsRayUI/Widgets/ScrollBar.hpp>
#include <LsRayUI/InputMan.hpp>


// MACROS //
#define SCROLLBAR_SIZE 16.0f


namespace LsRayUI
{
    // CONSTRUCTOR / DECONSTRUCTOR //
    ScrollBar::ScrollBar(enum Orientation pOrientation)
    {
        orientation = pOrientation;
        progress = 0;
        viewSize = 0;
        totalSize = -1;
        
        drawFocus = 0;
        
        emitProgressFlag = false;
        
        Signal::Connect(this, "mouse-down", &ScrollBar::SigMouseDownCb, this);
        Signal::Connect(this, "mouse-up", &ScrollBar::SigMouseUpCb, this);
    }
    
    ScrollBar::~ScrollBar()
    {
    
    }
    
    
    // PUBLIC METHODS //
    void ScrollBar::UpdateView(float pProgress, float pViewSize, float pTotalSize)
    {
        progress = Common::Clamp<float>(pProgress, 0, 1);
        viewSize = pViewSize;
        totalSize = pTotalSize;
        
        if (viewSize != viewSize || totalSize != totalSize)
        {
            viewSize = 0;
            totalSize = 0;
        }
        
        // Emit update signal
        if (!emitProgressFlag)
        {
            emitProgressFlag = true;
            Signal::Emit(
                this, "progress-update",
                "progress", Signal::AsParam(progress),
                nullptr
            );
            emitProgressFlag = false;
        }
    }
    
    void ScrollBar::Update(float delta)
    {
        // Update draw focus
        if (InputMan::GetHoveringWidget() == this || InputMan::GetCapturedWidget() == this)
            drawFocus = Common::Clamp<float>(drawFocus + delta * 10.0f, 0, 1);
        else
            drawFocus = Common::Clamp<float>(drawFocus - delta * 10.0f, 0, 1);
        
        UpdateSmoothProgress();
        
        if (std::abs(progress - smoothProgress) > 0.0001 &&
            totalSize > viewSize)
        {
            float step = delta * 10.0f;
            if (step > std::abs(smoothProgress - progress))
                step = std::abs(smoothProgress - progress);
            if (smoothProgress < progress)
                step = -step;
            
            UpdateView(progress + step, viewSize, totalSize);
        }
    }
    
    void ScrollBar::Draw()
    {
        if (viewSize >= totalSize)
            return;
        
        // Get rectangle
        Rectangle rect = Measure();
        if (!IsVisible(rect))
            return;
        
        Common::PushScissor(rect.x, rect.y, rect.width, rect.height);
        
        float size = SCROLLBAR_SIZE * (0.5f + 0.5f * drawFocus);
        bool isHorizontal = orientation == Orientation::HORIZONTAL;
        float maxlen = orientation == Orientation::HORIZONTAL ? rect.width : rect.height;
        float len = GetScrollBarLength(maxlen);
        
        DrawRectangleRounded(
            (Rectangle) {
                rect.x + ((rect.width - size) / 2.0f) * (!isHorizontal ? 1.0f : 0.0f),
                rect.y + ((rect.height - size) / 2.0f) * (isHorizontal ? 1.0f : 0.0f),
                isHorizontal ? maxlen : size,
                isHorizontal ? size : maxlen
            },
            1.0f,
            16,
            (Color){ 75, 75, 75, (unsigned char)(50.0f + 50.0f * drawFocus) }
        );
        
        float alpha = 155.0f + 50.0f * drawFocus;
        if (InputMan::GetCapturedWidget() == this)
            alpha = 255;
        
        float plusX = (rect.width - len) * progress * (isHorizontal ? 1.0f : 0.0f);
        float plusY = (rect.height - len) * progress * (!isHorizontal ? 1.0f : 0.0f);
        DrawRectangleRounded(
            (Rectangle) {
                rect.x + ((rect.width - size) / 2.0f) * (!isHorizontal ? 1.0f : 0.0f) + plusX,
                rect.y + ((rect.height - size) / 2.0f) * (isHorizontal ? 1.0f : 0.0f) + plusY,
                isHorizontal ? len : size,
                isHorizontal ? size : len
            },
            1.0f,
            16,
            (Color){ 255, 255, 255, (unsigned char)alpha }
        );
        
        Common::PopScissor();
    }
    
    
    // PRIVATE METHODS //
    void ScrollBar::SigMouseDownCb(void* self, Signal::sigparams_t* params)
    {
        reinterpret_cast<ScrollBar*>(self)->SigMouseDown(params);
    }
    void ScrollBar::SigMouseDown(Signal::sigparams_t* params)
    {
        if (Signal::GetParam<unsigned char>(params, "button") !=
            InputMan::MouseButton::LS_MOUSE_BUTTON_LEFT)
        {
            return;
        }
        
        if (CanCaptureInput())
            CaptureInput();
    }
    
    void ScrollBar::SigMouseUpCb(void* self, Signal::sigparams_t* params)
    {
        reinterpret_cast<ScrollBar*>(self)->SigMouseUp(params);
    }
    void ScrollBar::SigMouseUp(Signal::sigparams_t* params)
    {
        if (Signal::GetParam<unsigned char>(params, "button") !=
            InputMan::MouseButton::LS_MOUSE_BUTTON_LEFT ||
            !IsCapturingInput())
        {
            return;
        }
        
        ReleaseCaptureInput();
    }
    
    
    float ScrollBar::GetScrollBarLength()
    {
        Rectangle rect = Measure();
        
        if (orientation == Orientation::HORIZONTAL)
            return GetScrollBarLength(rect.width);
        else
            return GetScrollBarLength(rect.height);
    }
    
    float ScrollBar::GetScrollBarLength(float widgetLength)
    {
        if (widgetLength <= 0 || totalSize <= viewSize || totalSize <= 0)
            return SCROLLBAR_SIZE * 2;
        
        float len = Common::Clamp<float>(
            (viewSize / totalSize) * widgetLength,
            SCROLLBAR_SIZE * 2, 999999
        );
        return len;
    }
    
    void ScrollBar::UpdateSmoothProgress()
    {
        // Update scrollbar position
        if (InputMan::GetCapturedWidget() == this)
        {
            if (viewSize >= totalSize)
                return;
            
            Rectangle rect = Measure();
            if (!IsVisible(rect))
                return;
            
            // Get relative mouse pos
            Vector2 mpos = GetMousePosition();
            mpos.x -= rect.x;
            mpos.y -= rect.y;
            
            // Flip if vertical orientation
            // (always assume its horizontal)
            if (orientation == Orientation::VERTICAL)
            {
                mpos = (Vector2){ mpos.y, mpos.x };
                
                rect = (Rectangle){
                    rect.y, rect.x, rect.height, rect.width
                };
            }
            
            // Get scrollbar length
            float sbLength = GetScrollBarLength(rect.width);
            if (sbLength <= 0 || sbLength >= rect.width)
                return;
            
            mpos.x = Common::Clamp<float>(mpos.x, sbLength / 2.0f, rect.width - (sbLength / 2.0f));
            smoothProgress = (mpos.x - (sbLength / 2.0f)) / (rect.width - sbLength);
            smoothProgress = Common::Clamp<float>(smoothProgress, 0.0f, 1.0f);
        }
    }
}