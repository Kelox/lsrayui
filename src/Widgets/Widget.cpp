#include <LsRayUI/Widgets/Widget.hpp>
#include <LsRayUI/LsRayUI.hpp>
#include <LsRayUI/Common.hpp>
#include <LsRayUI/InputMan.hpp>


namespace LsRayUI
{
    // CONSTRUCTOR / DECONSTRUCTOR //
    Widget::Widget() : Widget(0, 0, 1, 1) { }
    
    Widget::Widget(float px, float py, float pWidth, float pHeight)
    {
        x = px;
        y = py;
        width = pWidth;
        height = pHeight;
        
        /*lMargin = 0;
        rMargin = 0;
        tMargin = 0;
        bMargin = 0;*/
        
        parent = nullptr;
        
        enabled = true;
        
        hoverCursor = MOUSE_CURSOR_DEFAULT;
    }
    
    Widget::~Widget()
    {
        // Is input captured?
        if (InputMan::GetCapturedWidget() == this)
        {
            TraceLog(LOG_ERROR, "Destroying widget that was still capturing input");
            ReleaseCaptureInput();
        }
        
        // Delete children
        for (int i = (int)childList.size() - 1; i >= 0; i--)
        {
            // Get child
            auto iter = childList.begin();
            std::advance(iter, i);
            Widget* child = *iter;
            
            // Unparent child
            child->Unparent();
            
            // Delete child
            delete child;
        }
    }
    
    
    // PUBLIC METHODS //
    Rectangle Widget::Measure()
    {
        // Get parent rect
        Rectangle parentRect = (Rectangle){
            0, 0,
            (float)GetScreenWidth(),
            (float)GetScreenHeight()
        };
        if (parent)
        {
            parentRect = parent->Measure();
            parent->MeasureChild(this);
        }
        else
        {
            // Is it the root?
            if (LsRayUI::GetRootWidget() == this)
            {
                x = 0;
                y = 0;
                width = (float)GetScreenWidth();
                height = (float)GetScreenHeight();
                return parentRect;
            }
        }
        
        // Get local self rect
        Rectangle selfRect = (Rectangle){
            x,// + lMargin,
            y,// + tMargin,
            //Common::Clamp<float>(width - lMargin - rMargin, 0, (float)GetScreenWidth()),
            width,
            //Common::Clamp<float>(height - tMargin - bMargin, 0, (float)GetScreenHeight())
            height
        };
        
        // Translate to parent
        selfRect.x += parentRect.x;
        selfRect.y += parentRect.y;
        
        // Done
        return selfRect;
    }
    
    Widget* Widget::GetWidgetAt(float posX, float posY, bool ignoreDisabled)
    {
        if (ignoreDisabled && !GetEnabled())
        {
            throw std::runtime_error(
                "Called GetWidgetAt() on disabled Widget, while ignoreDisabled == true"
            );
        }
        
        // Get absolute position
        Rectangle selfRect = Measure();
        posX += selfRect.x;
        posY += selfRect.y;
        
        for (int i = (int)childList.size() - 1; i >= 0; i--)
        {
            // Get child
            auto iter = childList.begin();
            std::advance(iter, i);
            Widget* child = *iter;
            
            // Skip if disabled?
            if (ignoreDisabled && !child->GetEnabled())
                continue;
            
            // Skip if it does not contain the point
            Rectangle childRect = child->Measure();
            if (!Common::IsPointInRect<float>(
                posX, posY,
                childRect.x, childRect.y, childRect.width, childRect.height))
            {
                continue;
            }
            
            // Find widget within this child
            return child->GetWidgetAt(posX - childRect.x, posY - childRect.y);
        }
        
        // Just return this
        return this;
    }
    
    Widget* Widget::GetParent()
    {
        return parent;
    }
    
    /*void Widget::SendEvent(Event& event)
    {
        HandleEvent(event);
    }*/
    
    void Widget::SetEnabled(bool state)
    {
        if (!state && IsCapturingInput())
            ReleaseCaptureInput();
        enabled = state;
    }
    
    bool Widget::GetEnabled()
    {
        return enabled;
    }
    
    bool Widget::IsVisible(Rectangle& rect)
    {
        return rect.width > 0 && rect.height > 0 &&
            Common::RectOverlapArea<float>(
                rect.x, rect.y, rect.width, rect.height,
                0, 0, (float)LsRayUI::GetScreenWidth(), (float)LsRayUI::GetScreenHeight()
            ) > 0;
    }
    
    bool Widget::IsVisible()
    {
        Rectangle rect = Measure();
        return IsVisible(rect);
    }
    
    /*void Widget::SetMargin(float pMargin)
    {
        SetMargin(pMargin, pMargin, pMargin, pMargin);
    }
    void Widget::SetMargin(float pLeft, float pRight, float pTop, float pBottom)
    {
        lMargin = pLeft;
        rMargin = pRight;
        tMargin = pTop;
        bMargin = pBottom;
    }*/
    
    void Widget::SetDefaultSize(float pWidth, float pHeight)
    {
        width = pWidth;
        height = pHeight;
    }
    
    int Widget::GetHoverCursor()
    {
        return hoverCursor;
    }
    
    
    // PROTECTED METHODS //
    void Widget::SetParent(Widget* pParent)
    {
        if (parent)
            Unparent();
        if (!pParent)
            return;
        
        pParent->AddChild(this);
    }
    void Widget::Unparent()
    {
        if (!parent)
            return;
        
        parent->RemoveChild(this);
    }
    
    void Widget::AddChild(Widget* pChild)
    {
        // Check if it exists already
        bool exists = false;
        for (Widget* c : childList)
        {
            if (c == pChild)
            {
                exists = true;
                break;
            }
        }
        
        // Return if it does
        if (exists)
        {
            TraceLog(LOG_ERROR, "Tried to add child widget twice");
            return;
        }
        
        // Add child
        childList.push_back(pChild);
        pChild->parent = this;
    }
    void Widget::RemoveChild(Widget* pChild)
    {
        for (auto iter = childList.begin(); iter != childList.end(); iter++)
        {
            Widget* child = *iter;
            if (child != pChild)
                continue;
            
            child->parent = nullptr;
            childList.erase(iter);
            OnRemoveChild(child);
            return;
        }
        
        TraceLog(LOG_ERROR, "Tried to remove a child that does not exist");
    }
    
    void Widget::OnRemoveChild(LsRayUI::Widget* child)
    {
        // Do nothing
    }
    
    Widget* Widget::GetChild(int index)
    {
        if (index < 0 || index >= childList.size())
            return nullptr;
        
        auto iter = childList.begin();
        std::advance(iter, index);
        return *iter;
    }
    
    int Widget::GetChildCount()
    {
        return childList.size();
    }
    
    
    bool Widget::CanCaptureInput()
    {
        return InputMan::GetCapturedWidget() == nullptr;
    }
    
    bool Widget::IsCapturingInput()
    {
        return InputMan::GetCapturedWidget() == this;
    }
    
    void Widget::CaptureInput()
    {
        if (!CanCaptureInput() && InputMan::GetCapturedWidget() != this)
        {
            
            throw std::runtime_error(
                "Could not capture input: another widget has already captured it"
            );
        }
        else if (!CanCaptureInput() && InputMan::GetCapturedWidget() == this)
        {
            
            throw std::runtime_error(
                "Could not capture input: input already captured"
            );
        }
        
        InputMan::SetCapturedWidget(this);
    }
    
    void Widget::ReleaseCaptureInput()
    {
        if (InputMan::GetCapturedWidget() != this)
        {
            throw std::runtime_error(
                "Tried to release another widget's capture"
            );
        }
        
        InputMan::SetCapturedWidget(nullptr);
    }
    
    void Widget::MeasureChild(Widget* child)
    {
        // Do nothing
    }
    
    void Widget::SetHoverCursor(int cursor)
    {
        hoverCursor = cursor;
    }
}