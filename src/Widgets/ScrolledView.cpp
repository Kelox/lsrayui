#include <LsRayUI/Widgets/ScrolledView.hpp>
#include <LsRayUI/Common.hpp>


// MACROS //
#define SCROLLBAR_SIZE 8.0f
#define SENSITIVITY 25.0f


namespace LsRayUI
{
    // CONSTRUCTOR / DECONSTRUCTOR //
    ScrolledView::ScrolledView(float pPadding, bool pStretchX, bool pStretchY)
    {
        padding = pPadding + SCROLLBAR_SIZE;
        stretchX = pStretchX;
        stretchY = pStretchY;
        scrollX = 0;
        scrollY = 0;
        scrollMaxX = 0;
        scrollMaxY = 0;
        
        Signal::Connect(this, "mouse-scroll", &ScrolledView::SigMouseScrollCb, this);
    }
    
    ScrolledView::~ScrolledView()
    {
    
    }
    
    
    // PUBLIC METHODS //
    void ScrolledView::Update(float delta)
    {
        // Chain update loop
        for (int i = 0; i < GetChildCount(); i++)
            Widget::GetChild(i)->Update(delta);
    }
    
    void ScrolledView::Draw()
    {
        // Skip if not visible
        Rectangle rect = Measure();
        if (!IsVisible(rect))
            return;
        
        // Draw child
        if (GetChildCount() > 0)
        {
            Common::PushScissor(
                rect.x + padding, rect.y + padding,
                rect.width - padding * 2.0f, rect.height - padding * 2.0f
            );
            GetChild()->Draw();
            Common::PopScissor();
        }
        
        // Draw scroll bars
        if (scrollMaxY > 0)
        {
            float sbHeight = std::max(
                (height / (scrollMaxY + height)) * height, SCROLLBAR_SIZE * 2.0f
            );
            DrawRectangleRounded(
                (Rectangle) {
                    rect.x + rect.width - SCROLLBAR_SIZE,
                    rect.y + (scrollY / scrollMaxY) * (height - sbHeight),
                    SCROLLBAR_SIZE,
                    sbHeight
                },
                1.0f,
                16,
                (Color) {255, 255, 255, 125}
            );
        }
        
        if (scrollMaxX > 0)
        {
            float sbWidth = std::max(
                (width / (scrollMaxX + width)) * width, SCROLLBAR_SIZE * 2.0f
            );
            DrawRectangleRounded(
                (Rectangle) {
                    rect.x + (scrollX / scrollMaxX) * (width - sbWidth),
                    rect.y + rect.height - SCROLLBAR_SIZE,
                    sbWidth,
                    SCROLLBAR_SIZE
                },
                1.0f,
                16,
                (Color) {255, 255, 255, 125}
            );
        }
    }
    
    Rectangle ScrolledView::Measure()
    {
        Rectangle rect = Widget::Measure();
        UpdateBounds();
        return rect;
    }
    
    void ScrolledView::SetChild(Widget* child)
    {
        if (!childList.empty())
        {
            throw std::runtime_error(
                "A ScrolledView may only have one child at a time"
            );
        }
        
        if (child)
            AddChild(child);
        else
            RemoveChild(child);
    }
    
    Widget* ScrolledView::GetChild()
    {
        return Widget::GetChild(0);
    }
    
    
    // PROTECTED METHODS //
    void ScrolledView::MeasureChild(Widget* child)
    {
        child->x = -scrollX + padding;
        child->y = -scrollY + padding;
        
        float stretchWidth = width - padding * 2.0f;
        float stretchHeight = height - padding * 2.0f;
        
        if (stretchX/* || child->width < stretchWidth*/)
            child->width = stretchWidth;
        if (stretchY/* || child->height < stretchHeight*/)
            child->height = stretchHeight;
    }
    
    
    // PRIVATE METHODS //
    void ScrolledView::UpdateBounds()
    {
        if (GetChildCount() < 1)
            return;
        
        Widget* child = GetChild();
        
        scrollMaxX = !stretchX ? std::max(scrollMaxX, child->width + padding * 2.0f) : 0;
        scrollMaxY = !stretchY ? std::max(scrollMaxY, child->height + padding * 2.0f) : 0;
        scrollMaxX = std::max(scrollMaxX - width, 0.0f);
        scrollMaxY = std::max(scrollMaxY - height, 0.0f);
        
        scrollX = Common::Clamp(scrollX, 0.0f, scrollMaxX);
        scrollY = Common::Clamp(scrollY, 0.0f, scrollMaxY);
    }
    
    void ScrolledView::SigMouseScrollCb(void* self, Signal::sigparams_t* params)
    {
        reinterpret_cast<ScrolledView*>(self)->SigMouseScroll(params);
    }
    void ScrolledView::SigMouseScroll(Signal::sigparams_t* params)
    {
        if (!CanCaptureInput())
            return;
        
        scrollX = Common::Clamp(
            scrollX + Signal::GetParam<float>(params, "dx") * SENSITIVITY, 0.0f, scrollMaxX
        );
        scrollY = Common::Clamp(
            scrollY + Signal::GetParam<float>(params, "dy") * -SENSITIVITY, 0.0f, scrollMaxY
        );
    }
}