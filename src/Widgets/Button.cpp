#include <LsRayUI/Widgets/Button.hpp>
#include <LsRayUI/Common.hpp>
#include <cstring>
#include <LsRayUI/InputMan.hpp>


// MACROS //
#define BUTTON_ROUNDNESS 8.0f
#define BUTTON_FONT_SIZE 24.0f
#define BUTTON_SHADOW_OFFSET 4.0f


namespace LsRayUI
{
    // CONSTRUCTOR / DECONSTRUCTOR //
    Button::Button(const char* pText)
    {
        text = std::string(pText);
        
        Signal::Connect(this, "mouse-down", &Button::SigMouseDownCb, this);
        Signal::Connect(this, "mouse-up", &Button::SigMouseUpCb, this);
        
        SetHoverCursor(MOUSE_CURSOR_POINTING_HAND);
    }
    
    Button::~Button()
    {
    
    }
    
    
    // PUBLIC METHODS //
    void Button::Update(float delta)
    {
    
    }
    
    void Button::Draw()
    {
        // Get rectangle
        Rectangle rect = Measure();
        if (!IsVisible(rect))
            return;
        
        // Determine roundness
        float roundness = rect.width < rect.height ? rect.width : rect.height;
        roundness = Common::Clamp<float>(
            (1.0f / roundness) * BUTTON_ROUNDNESS,
            0, 1
        );
        
        // Draw background
        DrawRectangleRounded(
            (Rectangle){
                rect.x + BUTTON_SHADOW_OFFSET,
                rect.y + BUTTON_SHADOW_OFFSET,
                rect.width, rect.height
            },
            roundness,
            16,
            (Color){ 0, 0, 0, 155 }
        );
        
        // Get color
        Color color = (Color){ 65, 160, 185, 255 };
        if (InputMan::GetCapturedWidget() == this)
            color = (Color){ 105, 200, 225, 255 };
        else if (InputMan::GetHoveringWidget() == this)
            color = (Color){ 85, 180, 205, 255 };
        
        DrawRectangleRounded(
            rect,
            roundness,
            16,
            color
        );
        
        Common::PushScissor(rect.x, rect.y, rect.width, rect.height);
        Common::DrawShadowedText(
            GetFontDefault(),
            text.c_str(),
            BUTTON_FONT_SIZE,
            rect.x + rect.width / 2.0f, rect.y + rect.height / 2.0f,
            1.0f,
            0.5f, 0.5f,
            2.0f,
            WHITE
        );
        Common::PopScissor();
    }
    
    
    // PRIVATE METHODS //
    void Button::SigMouseDownCb(void* self, Signal::sigparams_t* params)
    {
        reinterpret_cast<Button*>(self)->SigMouseDown(params);
    }
    void Button::SigMouseDown(Signal::sigparams_t* params)
    {
        if (Signal::GetParam<unsigned char>(params, "button") !=
            InputMan::MouseButton::LS_MOUSE_BUTTON_LEFT)
        {
            return;
        }
        
        if (CanCaptureInput())
            CaptureInput();
    }
    
    void Button::SigMouseUpCb(void* self, Signal::sigparams_t* params)
    {
        reinterpret_cast<Button*>(self)->SigMouseUp(params);
    }
    void Button::SigMouseUp(Signal::sigparams_t* params)
    {
        if (Signal::GetParam<unsigned char>(params, "button") !=
            InputMan::MouseButton::LS_MOUSE_BUTTON_LEFT ||
            !IsCapturingInput())
        {
            return;
        }
        
        ReleaseCaptureInput();
    }
}