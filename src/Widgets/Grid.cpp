#include <LsRayUI/Widgets/Grid.hpp>
#include <LsRayUI/Common.hpp>


namespace LsRayUI
{
    // CONSTRUCTOR / DECONSTRUCTOR //
    Grid::Grid(float pSpacing) :
        Widget(0, 0, 0, 0),
        spacing(pSpacing)
    {
    
    }
    
    Grid::~Grid()
    {
    
    }
    
    
    // PUBLIC METHODS //
    void Grid::Update(float delta)
    {
        // Chain update loop
        for (int i = 0; i < GetChildCount(); i++)
            Widget::GetChild(i)->Update(delta);
    }
    
    void Grid::Draw()
    {
        // Get rectangle
        Rectangle rect = Measure();
        if (!IsVisible(rect))
            return;
        
        // Draw children
        Common::PushScissor(rect.x, rect.y, rect.width, rect.height);
        for (int i = 0; i < GetChildCount(); i++)
            GetChild(i)->Draw();
        Common::PopScissor();
    }
    
    void Grid::SetChild(Widget* child, int row, int col, int rowSpan, int colSpan)
    {
        if (!child)
            throw std::runtime_error("Child must not be null");
        if (child->parent)
            throw std::runtime_error("Child has a parent already");
        
        gridalloc_t ga;
        ga.child = child;
        ga.row = row;
        ga.col = col;
        ga.rows = rowSpan;
        ga.cols = colSpan;
        gridAllocList.push_back(ga);
        
        AddChild(child);
    }
    
    
    // PROTECTED METHODS //
    void Grid::MeasureChild(Widget* child)
    {
        // Prepare grid allocation for this child
        gridalloc_t ga;
        ga.child = child;
        ga.col = 0;
        ga.row = 0;
        ga.cols = 1;
        ga.rows = 1;
        
        // Prepare boundary
        int minx = 999999;
        int maxx = -999999;
        int miny = 999999;
        int maxy = -999999;
        
        // ..foreach child
        for (int i = (int)gridAllocList.size() - 1; i >= 0; i--)
        {
            auto iter = gridAllocList.begin();
            std::advance(iter, i);
            gridalloc_t& phalloc = *iter;
            
            // Save grid allocated if its of the correct child
            if (phalloc.child == child)
                ga = phalloc;
            
            // Store boundaries
            minx = std::min(minx, phalloc.col);
            maxx = std::max(maxx, phalloc.col + phalloc.cols);
            miny = std::min(miny, phalloc.row);
            maxy = std::max(maxy, phalloc.row + phalloc.rows);
        }
        
        // Determine local pixel location
        int cols = maxx - minx;
        int rows = maxy - miny;
        float cellW = (width - spacing * (float)(cols + 1)) / (float)cols;
        float cellH = (height - spacing * (float)(rows + 1)) / (float)rows;
        
        // Apply location
        child->x = (spacing + cellW) * (float)ga.col + spacing;
        child->y = (spacing + cellH) * (float)ga.row + spacing;
        child->width = cellW * (float)ga.cols + spacing * (float)(ga.cols - 1);
        child->height = cellH * (float)ga.rows + spacing * (float)(ga.rows - 1);
    }
    
    void Grid::OnRemoveChild(Widget* child)
    {
        for (int i = (int)gridAllocList.size() - 1; i >= 0; i--)
        {
            auto iter = gridAllocList.begin();
            std::advance(iter, i);
            gridalloc_t& ga = *iter;
            
            if (ga.child == child)
            {
                gridAllocList.erase(iter);
                break;
            }
        }
    }
}