#include <raylib.h>
#include <LsRayUI/LsRayUI.hpp>


int main(int argc, char** argv)
{
    InitWindow(800, 600, "LsRayUI Test");
    SetWindowMinSize(400, 300);
    SetWindowState(FLAG_WINDOW_RESIZABLE);
    SetTargetFPS(144);
    
    LsRayUI::Init();
    LsRayUI::Window window(100, 100, 400, 300);
    LsRayUI::Window* subwindow = new LsRayUI::Window();
    
    LsRayUI::Grid* grid = new LsRayUI::Grid(16.0f);
    LsRayUI::Window* testwindow[4];
    for (int i = 0; i < 4; i++)
        testwindow[i] = new LsRayUI::Window();
    grid->SetChild(testwindow[0], 0, 0, 1, 2);
    grid->SetChild(testwindow[1], 1, 0, 1, 1);
    grid->SetChild(testwindow[2], 1, 1, 1, 1);
    grid->SetChild(testwindow[3], 2, 0, 1, 3);
    
    testwindow[0]->SetChild(new LsRayUI::Entry("Random ass text i dont\nfucking know\nabcabc\nabcabc\nabcabc\n", "Hint", false));
    testwindow[0]->SetPadding(8.0f);
    
    LsRayUI::Grid* grid2 = new LsRayUI::Grid(16.0f);
    testwindow[1]->SetChild(grid2);
    LsRayUI::Button* btn = new LsRayUI::Button("Test 123 456 789 101112");
    grid2->SetChild(btn, 0, 0, 1, 1);
    
    LsRayUI::Grid* grid3 = new LsRayUI::Grid(8.0f);
    auto sb = new LsRayUI::ScrollBar(LsRayUI::Orientation::HORIZONTAL);
    auto sb2 = new LsRayUI::ScrollBar(LsRayUI::Orientation::VERTICAL);
    sb->UpdateView(0.2f, 20.0f, 100.0f);
    sb2->UpdateView(0.2f, 30.0f, 100.0f);
    grid3->SetChild(sb, 0, 0, 1, 2);
    grid3->SetChild(sb2, 0, 2, 4, 1);
    grid3->SetChild(new LsRayUI::Button("Test 2"), 1, 0, 1, 2);
    grid3->SetChild(new LsRayUI::Button("Test 3"), 3, 0, 1, 2);
    grid3->SetChild(new LsRayUI::Button("Test 4"), 4, 0, 1, 2);
    grid3->SetChild(new LsRayUI::Button("Test 5"), 5, 0, 1, 2);
    grid3->SetChild(new LsRayUI::Button("Test 6"), 6, 0, 1, 2);
    grid3->SetChild(new LsRayUI::Button("Test 7"), 7, 0, 2, 2);
    grid3->SetChild(new LsRayUI::Button("Test 8"), 9, 0, 1, 1);
    grid3->SetChild(new LsRayUI::Button("Test 9"), 9, 1, 1, 1);
    grid3->SetDefaultSize(300, 450);
    LsRayUI::ScrolledView* sview = new LsRayUI::ScrolledView(0.0f, false, false);
    sview->SetChild(grid3);
    testwindow[2]->SetChild(sview);
    
    LsRayUI::ScrolledView* sview2 = new LsRayUI::ScrolledView(8.0f, false, false);
    auto entry = new LsRayUI::Entry("", "This is my hint", true);
    entry->SetDefaultSize(350, 50);
    sview2->SetChild(entry);
    testwindow[3]->SetChild(sview2);
    
    //subwindow->SetMargin(16.0f);
    subwindow->SetChild(grid);
    window.SetChild(subwindow);
    LsRayUI::SetRootWidget(&window);
    
    while (!WindowShouldClose())
    {
        BeginDrawing();
        ClearBackground(WHITE);
        
        float delta = GetFrameTime();
        LsRayUI::SetScreenSize(GetScreenWidth(), GetScreenHeight());
        LsRayUI::Update(delta);
        LsRayUI::Draw();
        
        DrawFPS(10, 10);
        EndDrawing();
    }
    
    LsRayUI::RemoveRootWidget();
    LsRayUI::Free();
    
    return 0;
}