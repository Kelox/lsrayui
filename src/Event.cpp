#include <LsRayUI/Event.hpp>


namespace LsRayUI
{
    Event::Event(LsRayUI::EventType pType)
    {
        type = pType;
    }
    
    MouseDownEvent::MouseDownEvent(float px, float py, MouseButton pButton) :
        Event(EventType::EVENT_TYPE_MOUSE_DOWN),
        x(px),
        y(py),
        button(pButton)
    {
    
    }
    
    MouseUpEvent::MouseUpEvent(float px, float py, MouseButton pButton) :
        Event(EventType::EVENT_TYPE_MOUSE_UP),
        x(px),
        y(py),
        button(pButton)
    {
    
    }
    
    MouseClickEvent::MouseClickEvent(float px, float py, MouseButton pButton) :
        Event(EventType::EVENT_TYPE_MOUSE_UP),
        x(px),
        y(py),
        button(pButton)
    {
    
    }
}