#pragma once


// INCLUDES //
#include <cstdarg>
#include <cstring>
#include <stdexcept>
#include <cstdint>


// MACROS //
#define SIG_NAME_LEN 64
#define SIG_PARAM_NAME_LEN 64


namespace LsRayUI::Signal
{
    // STRUCTS //
    typedef struct _sigparams_t sigparams_t;
    struct _sigparams_t
    {
        char name[SIG_PARAM_NAME_LEN];
        uintptr_t value;
        sigparams_t* next;
    };
    
    
    // TYPEDEFS //
    typedef void (*callbackFunc)(void* self, sigparams_t* params);
    
    
    // PROTOTYPES //
    
    void Connect(void* source, const char* name, callbackFunc targetFn, void* target);
    
    void Disconnect(void* source, const char* name, void* target);
    
    /// @brief Synchronously emits the signal.
    /// It is required to pack all parameter values inside Signal::AsParam(..).
    /// Usage example: Signal::Emit(myButton, "click",
    ///     "mouse-x", Signal::AsParam(16.7),
    ///     "mouse-y", Signal::AsParam(21.2), nullptr);
    void Emit(void* source, const char* name, ...);
    
    void Emit(void* source, const char* name, va_list list);
    
    /// @brief Retrieves the parameter value of a param list and casts it to the given type.
    /// This may be used from within a callback.
    template <typename T>
    T GetParam(sigparams_t* params, const char* name)
    {
        while (params != nullptr && strncmp(params->name, name, SIG_PARAM_NAME_LEN) != 0)
            params = params->next;
        if (params == nullptr)
        {
            char errmsg[256];
            snprintf(errmsg, 256, "Parameter '%s' does not exist", name);
            throw std::runtime_error(errmsg);
        }
        T retval;
        memcpy(&retval, &params->value, std::min(sizeof(T), sizeof(uintptr_t)));
        return retval;
    }
    
    /// @brief Checks if a parameter exists
    bool HasParam(sigparams_t* params, const char* name);
    
    template <typename T>
    uintptr_t AsParam(T value)
    {
        if (sizeof(T) > sizeof(uintptr_t))
            throw std::runtime_error("Invalid size");
        uintptr_t retval;
        memcpy(&retval, &value, std::min(sizeof(T), sizeof(uintptr_t)));
        return retval;
    }
    
    /// @brief Cleans up all signals that were never disconnected
    void Finalize();
}