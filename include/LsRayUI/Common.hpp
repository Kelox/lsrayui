#pragma once

#ifndef EXPORT
#if defined(_WIN32) || defined(WIN32)
#define EXPORT __declspec(dllexport)
#else
#define EXPORT
#endif
#endif

#define LS_MIN(a, b) ((a) < (b) ? (a) : (b))
#define LS_MAX(a, b) ((a) > (b) ? (a) : (b))


#include <raylib.h>
#include <string>


namespace LsRayUI::Common
{
    template <typename T>
    T Clamp(T value, T minv, T maxv)
    {
        if (value < minv)
            return minv;
        else if (value > maxv)
            return maxv;
        return value;
    }
    
    template <typename T>
    bool IsPointInRect(T px, T py, T rectX, T rectY, T rectW, T rectH)
    {
        return px >= rectX && py >= rectY && px < (rectX + rectW) && py < (rectY + rectH);
    }
    
    template <typename T>
    T RectOverlapArea(T rx1, T ry1, T rw1, T rh1, T rx2, T ry2, T rw2, T rh2)
    {
        T left = LS_MAX(rx1, rx2);
        T right = LS_MIN(rx1 + rw1, rx2 + rw2);
        T top = LS_MAX(ry1, ry2);
        T bot = LS_MIN(ry1 + rh1, ry2 + rh2);
        if (left > right || top > bot)
            return 0;
        
        return (right - left) * (bot - top);
    }
    
    void PushScissor(float x, float y, float w, float h);
    
    void PopScissor();
    
    bool HasScissor();
    
    /// @brief Distance between points
    float Dist2d(float x1, float y1, float x2, float y2);
    
    /// @param localPos (0|0) == top-left is at (posX|posY); (1|1) == bottom-right is at (posX|posY)
    void DrawShadowedText(Font font, const char* text, float fontSize,
                          float posX, float posY, float spacing,
                          float localPosX, float localPosY,
                          float shadowOffset, Color tint);
    
    void DrawMultilineText(Font font, float fontSize, float fontHeight, float spacing,
                           const char* text, float posX, float posY, Color tint);
    
    Vector2 MeasureMultilineText(Font font, float fontSize, float fontHeight, float spacing,
                                 const char* text);
    
    /// @brief Retrieves a specific line of a larger string. \n will be replaced by \0.
    /// @param str The complete string
    /// @param line The line number, starting from 0
    /// @param outStart optional output start index (inclusive)
    /// @param outEnd optional output end index (exclusive, points at \0 or \n)
    std::string GetStringLine(std::string& str, int line,
                              int* outStart = nullptr, int* outEnd = nullptr);
    
    /// @brief Retrieves the amount of lines
    int GetStringLineCount(std::string& str);
    
    int GetStringLineAtIndex(std::string& str, int idx);
}