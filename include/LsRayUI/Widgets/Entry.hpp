#pragma once


// INCLUDES //
#include <LsRayUI/Widgets/Widget.hpp>
#include <LsRayUI/Signal.hpp>


namespace LsRayUI
{
    class Entry : public Widget
    {
        private:
            std::string text;
            std::string hint;
            bool singleLine;
            bool hidden;
            
            struct {
                float left;
                float right;
                float top;
                float bottom;
            } padding;
            
            int selection[2];
            bool selectionBlink;
            float selectionBlinkTimer;
            
            int prevTextLength;
            float txtWidth, txtHeight;
            
        public:
            Entry(const char* pText, const char* pHint, bool pSingleLine);
            ~Entry();
            
            void Update(float delta) override;
            
            void Draw() override;
            
            void SetHidden(bool pHidden);
            
            bool GetHidden();
            
        private:
            int GetCharPosition(float pixelX, float pixelY);
            
            void GetPixelPosition(int charPos, float* outPixelX, float* outPixelY);
            
            static void SigMouseMoveCb(void* self, Signal::sigparams_t* params);
            void SigMouseMove(Signal::sigparams_t* params);
            
            static void SigMouseDownCb(void* self, Signal::sigparams_t* params);
            void SigMouseDown(Signal::sigparams_t* params);
            
            static void SigMouseUpCb(void* self, Signal::sigparams_t* params);
            void SigMouseUp(Signal::sigparams_t* params);
            
            static void SigKeyInputCb(void* self, Signal::sigparams_t* params);
            void SigKeyInput(Signal::sigparams_t* params);
            
            int GetLineOfIndex(int idx);
    };
}