#pragma once


// INCLUDES //
#include <LsRayUI/Widgets/Widget.hpp>
#include <LsRayUI/Signal.hpp>
#include <string>


namespace LsRayUI
{
    class Button : public Widget
    {
        private:
            std::string text;
            
            
        public:
            Button(const char* pText);
            ~Button();
            
            /// @brief Update loop
            void Update(float delta) override;
            
            /// @brief Draw loop
            void Draw() override;
            
        private:
            static void SigMouseDownCb(void* self, Signal::sigparams_t* params);
            void SigMouseDown(Signal::sigparams_t* params);
            
            static void SigMouseUpCb(void* self, Signal::sigparams_t* params);
            void SigMouseUp(Signal::sigparams_t* params);
    };
}