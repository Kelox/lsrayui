#pragma once


// INCLUDES //
#include <LsRayUI/Widgets/Widget.hpp>
#include <LsRayUI/InputMan.hpp>
#include <LsRayUI/Signal.hpp>


namespace LsRayUI
{
    class ScrolledView : public Widget
    {
        private:
            float scrollX, scrollY;
            float scrollMaxX, scrollMaxY;
            
            float padding;
            bool stretchX, stretchY;
            
        public:
            ScrolledView(float pPadding, bool pStretchX, bool pStretchY);
            ~ScrolledView();
            
            /// @brief Update loop
            void Update(float delta) override;
            
            /// @brief Draw loop
            void Draw() override;
            
            /// @brief Does mostly the same as Widget::Measure
            Rectangle Measure() override;
            
            /// @brief Sets the child.
            /// This takes ownership of child.
            /// The caller takes ownership of the previously set child. You must
            /// have retrieved it using GetChild() beforehand.
            /// @param child Pointer to a widget or null
            void SetChild(Widget* child);
            
            /// @brief Retrieves the currently set child.
            /// Data is owned by this.
            Widget* GetChild();
            
        protected:
            void MeasureChild(Widget* child) override;
        
        private:
            void UpdateBounds();
            
            static void SigMouseScrollCb(void* self, Signal::sigparams_t* params);
            void SigMouseScroll(Signal::sigparams_t* params);
    };
}