#pragma once


// INCLUDES //
#include <LsRayUI/Widgets/Widget.hpp>
#include <list>


namespace LsRayUI
{
    // STRUCTS //
    typedef struct
    {
        Widget* child;
        int row, col;
        int rows, cols;
    } gridalloc_t;
    
    
    // GRID CLASS //
    class Grid : public Widget
    {
        private:
            std::list<gridalloc_t> gridAllocList;
            float spacing;
        
        public:
            Grid(float pSpacing = 0);
            ~Grid();
            
            /// @brief Update loop
            void Update(float delta) override;
            
            /// @brief Draw loop
            void Draw() override;
            
            void SetChild(Widget* child, int row, int col, int rowSpan, int colSpan);
        
        protected:
            void MeasureChild(Widget* child) override;
            
            void OnRemoveChild(Widget* child) override;
    };
}