#pragma once


// INCLUDES //
#include <list>
#include <raylib.h>
#include <stdexcept>
#include <LsRayUI/Signal.hpp>


// SIGNALS //
// mouse-hover(float x, float y)
// mouse-unhover(float x, float y)
// mouse-move(float x, float y, float dx, float dy)
// mouse-down(float x, float y, InputMan::MouseButton button)
// mouse-up(float x, float y, InputMan::MouseButton button)
// mouse-click(float x, float y, InputMan::MouseButton button)
// mouse-scroll(float x, float y, float dx, float dy)
// key-down(int keycode, char keychar)
// key-up(int keycode, char keychar)
// key-input(int keycode, char keychar)


namespace LsRayUI
{
    // CLASS //
    class Widget
    {
        private:
            int hoverCursor;
            
        protected:
            // Position relative to parent
            float x, y;
            float width, height;
            
            // Widget tree
            Widget* parent;
            std::list<Widget*> childList;
            
            // Status
            bool enabled;
        
        public:
            friend class Window;
            friend class Grid;
            friend class ScrolledView;
            
            Widget();
            Widget(float px, float py, float pWidth, float pHeight);
            ~Widget();
            
            /// @brief Retrieves the absolute screen location of this widget
            virtual Rectangle Measure();
            
            /// @brief Returns either this or a child widget.
            /// @param posX x coord relative to this
            /// @param posY y coord relative to this
            /// @param ignoreDisabled If true, disabled Widgets will be ignored
            Widget* GetWidgetAt(float posX, float posY, bool ignoreDisabled = true);
            
            /// @brief Retrieves the parent widget
            Widget* GetParent();
            
            /// @brief Used internally to notify about eg. mouse input.
            /// This only calls HandleEvent(..).
            //void SendEvent(Event& event);
            
            /// @brief Update loop
            virtual void Update(float delta) = 0;
            
            /// @brief Draw loop
            virtual void Draw() = 0;
            
            void SetEnabled(bool state);
            bool GetEnabled();
            
            /// @brief Check whether the given rectangle would be visible.
            /// The rectangle is retrieved via Measure()
            bool IsVisible(Rectangle& rect);
            bool IsVisible();
            
            //void SetMargin(float pMargin);
            //void SetMargin(float pLeft, float pRight, float pTop, float pBottom);
            
            /// @brief Sets the default size of this widget.
            /// Note: most parents continuously change the child's location anyways, so this
            /// may only be used for parents which do not do so. Eg. ScrolledView.
            void SetDefaultSize(float pWidth, float pHeight);
            
            /// @brief Gets the request hover cursor
            int GetHoverCursor();
            
        protected:
            /// @brief pParent owns this
            void SetParent(Widget* pParent);
            /// @brief Caller owns this
            void Unparent();
            
            /// @brief this owns pChild
            void AddChild(Widget* pChild);
            /// @brief Gives ownership of this instance to the caller
            void RemoveChild(Widget* pChild);
            
            /// @brief is called right after the child was removed
            virtual void OnRemoveChild(Widget* child);
            
            /// @brief Retrieves the n-th child or null
            Widget* GetChild(int index);
            
            /// @brief Retrieves the amount of children
            int GetChildCount();
            
            /// @brief Returns true if the input can be captured
            bool CanCaptureInput();
            
            /// @brief Returns true if this widget is currently capturing input
            bool IsCapturingInput();
            
            /// @brief Captures input.
            /// You must check CanCaptureInput() first.
            /// You must release the capture eventually.
            void CaptureInput();
            
            /// @brief Releases input capure.
            void ReleaseCaptureInput();
            
            /// @brief Called before child is about to be measured. Layout widgets
            /// (eg. Grids) may update child's position/size here.
            /// Nothing happens here by default.
            virtual void MeasureChild(Widget* child);
            
            /// @brief Sets the request hover cursor
            void SetHoverCursor(int cursor);
    };
}