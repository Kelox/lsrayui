#pragma once


// INCLUDES //
#include <LsRayUI/Widgets/Widget.hpp>


namespace LsRayUI
{
    class Window : public Widget
    {
        private:
            float padding;
            
        public:
            Window();
            Window(float px, float py, float pWidth, float pHeight);
            ~Window();
            
            /// @brief Update loop
            void Update(float delta) override;
            
            /// @brief Draw loop
            void Draw() override;
            
            /// @brief Sets the child.
            /// This takes ownership of child.
            /// The caller takes ownership of the previously set child. You must
            /// have retrieved it using GetChild() beforehand.
            /// @param child Pointer to a widget or null
            void SetChild(Widget* child);
            
            /// @brief Retrieves the currently set child.
            /// Data is owned by this.
            Widget* GetChild();
            
            /// @brief Sets the child padding
            void SetPadding(float pPadding);
        
        protected:
            void MeasureChild(Widget* child) override;
    };
}