#pragma once


// INCLUDES //
#include <LsRayUI/Widgets/Widget.hpp>
#include <LsRayUI/Signal.hpp>
#include <LsRayUI/Types.hpp>


// SIGNALS //
// progress-update(float progress)


namespace LsRayUI
{
    class ScrollBar : public Widget
    {
        private:
            enum Orientation orientation;
            float progress;
            float smoothProgress;
            float viewSize; // eg. you see 20 pixels (= viewSize)..
            float totalSize; // ..of a 100px high image (= totalSize)
            
            float drawFocus;
            
            bool emitProgressFlag; // true while emitting to avoid recursion
        
        public:
            ScrollBar(enum Orientation pOrientation);
            ~ScrollBar();
            
            /// @brief Updates the scrollbar. If viewSize <= totalSize, the scrollbar is hidden
            /// @param pProgress Progress in [0; 1]
            /// @param pViewSize eg. visible 20px of a larger space
            /// @param pTotalSize eg. 100px. ViewSize is part of those 100px
            void UpdateView(float pProgress, float pViewSize, float pTotalSize);
            
            /// @brief Update loop
            void Update(float delta) override;
            
            /// @brief Draw loop
            void Draw() override;
            
            
        private:
            static void SigMouseDownCb(void* self, Signal::sigparams_t* params);
            void SigMouseDown(Signal::sigparams_t* params);
            
            static void SigMouseUpCb(void* self, Signal::sigparams_t* params);
            void SigMouseUp(Signal::sigparams_t* params);
            
            float GetScrollBarLength();
            float GetScrollBarLength(float widgetLength);
            
            void UpdateSmoothProgress();
    };
}