#pragma once


// INCLUDES //
#include <LsRayUI/Common.hpp>
//#include <LsRayUI/Event.hpp>
#include <LsRayUI/Signal.hpp>
#include <LsRayUI/Widgets/Widget.hpp>
#include <LsRayUI/Widgets/Window.hpp>
#include <LsRayUI/Widgets/Grid.hpp>
#include <LsRayUI/Widgets/Button.hpp>
#include <LsRayUI/Widgets/ScrollBar.hpp>
#include <LsRayUI/Widgets/ScrolledView.hpp>
#include <LsRayUI/Widgets/Entry.hpp>


namespace LsRayUI
{
    // PROTOTYPES //
    
    EXPORT void Init();
    
    EXPORT void Free();
    
    EXPORT void Update(float delta);
    
    EXPORT void Draw();
    
    EXPORT void SetScreenSize(int width, int height);
    EXPORT int GetScreenWidth();
    EXPORT int GetScreenHeight();
    
    
    /// @brief Returns true if there is a root widget
    EXPORT bool HasRootWidget();
    
    /// @brief Retrieves the current root widget.
    /// The data is owned by this function.
    EXPORT Widget* GetRootWidget();
    
    /// @brief Sets the new root widget.
    /// This function takes ownership of all objects within this widgets hierarchy.
    /// "widget" must not be null.
    // The previous root must have been removed first.
    EXPORT void SetRootWidget(Widget* widget);
    
    /// @brief Removes the current root widget. Check HasRootWidget() beforehand.
    /// The caller takes ownership of the data.
    /// Note: deleting a parent also deletes its children
    /// @return The former root widget
    EXPORT Widget* RemoveRootWidget();
}