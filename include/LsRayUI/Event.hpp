#pragma once


namespace LsRayUI
{
    // ENUMS //
    enum EventType
    {
        EVENT_TYPE_NONE = 0,
        EVENT_TYPE_MOUSE_DOWN,
        EVENT_TYPE_MOUSE_UP,
        EVENT_TYPE_MOUSE_CLICK,
    };
    enum MouseButton
    {
        MOUSE_BUTTON_NONE = 0,
        MOUSE_BUTTON_LEFT = 1 << 0,
        MOUSE_BUTTON_MIDDLE = 1 << 1,
        MOUSE_BUTTON_RIGHT = 1 << 2,
    };
    
    
    // EVENT CLASS //
    class Event
    {
        public:
            EventType type;
            
            explicit Event(EventType pType);
    };
    
    // MOUSE DOWN EVENT CLASS //
    class MouseDownEvent : public Event
    {
        public:
            float x, y;
            MouseButton button;
            
            MouseDownEvent(float px, float py, MouseButton pButton);
    };
    
    // MOUSE UP EVENT CLASS //
    class MouseUpEvent : public Event
    {
        public:
            float x, y;
            MouseButton button;
            
            MouseUpEvent(float px, float py, MouseButton pButton);
    };
    
    // MOUSE CLICK EVENT CLASS //
    class MouseClickEvent : public Event
    {
        public:
            float x, y;
            MouseButton button;
            
            MouseClickEvent(float px, float py, MouseButton pButton);
    };
}