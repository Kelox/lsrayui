#pragma once


// INCLUDES //
#include <raylib.h>
#include <LsRayUI/Signal.hpp>
#include <LsRayUI/Widgets/Widget.hpp>


namespace LsRayUI::InputMan
{
    // ENUMS //
    enum MouseButton : unsigned char
    {
        LS_MOUSE_BUTTON_NONE = 0,
        LS_MOUSE_BUTTON_LEFT = 1 << 0,
        LS_MOUSE_BUTTON_MIDDLE = 1 << 1,
        LS_MOUSE_BUTTON_RIGHT = 1 << 2,
    };
    
    
    // PROTOTYPES //
    void Init();
    
    void Free();
    
    void Update(float delta);
    
    void SetCapturedWidget(Widget* widget);
    Widget* GetCapturedWidget();
    
    Widget* GetHoveringWidget();
}