#pragma once


#include <LsRayUI/Common.hpp>


namespace LsRayUI
{
    // ENUMS //
    EXPORT enum Orientation
    {
        HORIZONTAL = 0,
        VERTICAL
    };
}