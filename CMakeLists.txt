cmake_minimum_required(VERSION 3.14)
project(LsRayUI)


add_library(
    LsRayUI
    SHARED
    src/LsRayUI.cpp
    src/Event.cpp
    src/Signal.cpp
    src/InputMan.cpp
    src/Widgets/Widget.cpp
    src/Widgets/Window.cpp
    src/Widgets/Grid.cpp
    src/Widgets/Button.cpp
    src/Widgets/ScrollBar.cpp
    src/Widgets/ScrolledView.cpp
    src/Widgets/Entry.cpp
    src/Common.cpp
    src/test.cpp
)

target_include_directories(
    LsRayUI
    PUBLIC
    include/
)


add_executable(
    LsRayUITest
    src/test.cpp
)

target_link_libraries(
    LsRayUITest
    LsRayUI
    raylib
)